#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE RabbitTests

#include <boost/test/data/test_case.hpp>
#include <boost/test/included/unit_test.hpp>

#include "Tree.hpp"

#include <iostream>
#include <random>
#include <sstream>

namespace ut = boost::unit_test;

auto &getRandomEngine() {
  static std::mt19937_64 randomEngine(std::random_device{}());
  return randomEngine;
}

auto generateDatasets(std::initializer_list<std::size_t> sizes) {
  std::vector<std::vector<std::uint64_t>> datasets;
  datasets.reserve(sizes.size());

  std::uniform_int_distribution<std::uint64_t> distribution;

  for (const auto &size : sizes) {
    std::vector<std::uint64_t> dataset;
    dataset.reserve(size);

    std::generate_n(std::back_inserter(dataset), size,
                    [&distribution]() { return distribution(getRandomEngine()); });

    datasets.push_back(std::move(dataset));
  }

  return std::move(datasets);
}

BOOST_TEST_DONT_PRINT_LOG_VALUE(std::vector<std::uint64_t>)

BOOST_DATA_TEST_CASE(InsertRandomElements,
                     ut::data::make(generateDatasets({10, 100, 1000, 10000, 100000, 1000000})),
                     keys) {
  Tree<std::uint64_t, std::string> tree;

  for (const auto &key : keys) {
    tree[key] = std::to_string(key);
  }

  auto shuffledKeys = keys;
  std::shuffle(shuffledKeys.begin(), shuffledKeys.end(), getRandomEngine());

  std::ostringstream expected, actual;

  for (const auto &key : shuffledKeys) {
    expected << key << "->" << std::to_string(key) << ";";
    actual << key << "->" << tree.at(key) << ";";
  }

  BOOST_REQUIRE_EQUAL(expected.str(), actual.str());
}

BOOST_DATA_TEST_CASE(RemoveElements,
                     ut::data::make(generateDatasets({10, 100, 1000, 10000, 100000, 1000000})),
                     keys) {
  Tree<std::uint64_t, std::string> tree;

  for (const auto &key : keys) {
    tree[key] = std::to_string(key);
  }

  auto shuffledKeys = keys;
  std::shuffle(shuffledKeys.begin(), shuffledKeys.end(), getRandomEngine());

  for (const auto &key : shuffledKeys) {
    tree.erase(key);
  }

  BOOST_REQUIRE_EQUAL(tree.size(), 0);
  BOOST_REQUIRE(tree.empty());
}

BOOST_DATA_TEST_CASE(ClearTree,
                     ut::data::make(generateDatasets({10, 100, 1000, 10000, 100000, 1000000})),
                     keys) {
  Tree<std::uint64_t, std::string> tree;

  for (const auto &key : keys) {
    tree[key] = std::to_string(key);
  }

  tree.clear();

  BOOST_REQUIRE_EQUAL(tree.size(), 0);
  BOOST_TEST(tree.empty());
}
