#ifndef RABBIT_TREE_HPP
#define RABBIT_TREE_HPP

#include "Node.hpp"

#include <cstdint>
#include <functional>
#include <queue>
#include <stdexcept>
#include <utility>

/**
 * Sorted associative container that contains key-value pairs with unique keys.
 * Implemented as red-black tree.
 * @tparam K Key type
 * @tparam V Value type
 * @tparam Compare The function type that compares the keys. Meets C++
 * standard library Compare requirements.
 */
template <class K, class V, class Compare = std::less<K>> class Tree {
public:
  /**
   * Constructs an empty container.
   */
  Tree() : _root(nullptr), _size(0), _less() {}

  /**
   * Constructs the container with the copy of the contents of another
   * container. Complexity is linear in the size of another container.
   * @param other Another container to be used as source to copy the elements of
   * the container from
   */
  Tree(const Tree &other)
      : _root(other._root == nullptr ? nullptr : other._root->copy()), _size(0), _less() {}

  /**
   * Constructs the container with the contents of other container using move
   * semantics.
   * Complexity is constant.
   * @param other Container to be used as source to move the elements of the
   * container from
   */
  Tree(Tree &&other) noexcept : _root(other._root), _size(0), _less() { other._root = nullptr; }

  /**
   * Destructs the container and its contents.
   */
  ~Tree() { delete _root; }

  /**
   * Erases all elements from the container.
   * Complexity is linear in the size of the container.
   */
  void clear() {
    setRoot(nullptr);
    _size = 0;
  }

  /**
   * Checks if there is an element with key equivalent to the specified key in
   * the container. Complexity is logarithmic in the size of another container.
   * @param key Key value of the element to search for
   * @return true if there is such an element, otherwise false
   */
  bool contains(const K &key) const {
    const auto [position, node] = findNode(key);
    return position == 0 and node != nullptr;
  }

  /**
   * Returns the number of elements in the container.
   * Complexity is constant.
   * @return The number of elements in the container
   */
  [[nodiscard]] std::size_t size() const { return _size; }

  /**
   * Checks if the container has no elements.
   * Complexity is constant.
   * @return true if the container has no elements, otherwise false
   */
  [[nodiscard]] bool empty() const { return root() == nullptr; }

  /**
   * Returns a reference to the mapped value of the element with key equivalent
   * to the specified key. Throws std::out_of_range if no such element exists.
   * Complexity is logarithmic in the size of the container.
   * @param key The key of the element to find
   * @return Reference to the mapped value of the requested element
   */
  V &at(const K &key) {
    auto [position, node] = findNode(key);

    if (position == 0 and node != nullptr) {
      return node->value();
    } else {
      throw std::out_of_range("Cannot find element with specified key");
    }
  }

  /**
   * Returns a reference to the mapped value of the element with key equivalent
   * to the specified key. Throws std::out_of_range if no such element exists.
   * Complexity is logarithmic in the size of the container.
   * @param key The key of the element to find
   * @return Reference to the mapped value of the requested element
   */
  const V &at(const K &key) const {
    const auto [position, node] = findNode(key);

    if (position == 0 and node != nullptr) {
      return node->value();
    } else {
      throw std::out_of_range("Cannot find element with specified key");
    }
  }

  /**
   * Inserts element into the container, if the container doesn't already
   * contains an element with an equivalent key. Complexity is
   * logarithmic in the size of the container.
   * @param key The key of the element to insert
   * @param value The value of the element to insert
   * @return Returns a pair consisting of a reference to the to the mapped value
   * of the inserted element (or to the element that prevented the insertion)
   * and a bool denoting whether the insertion took place
   */
  std::pair<V &, bool> insert(const K &key, const V &value) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(key, value);
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return {newNode->value(), true};
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(key, value);
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return {newNode->value(), true};
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(key, value));

      fixInsert(root());
      _size += 1;

      return {root()->value(), true};
    } else {
      return {node->value(), false};
    }
  }

  /**
   * Inserts element into the container, if the container doesn't already
   * contains an element with an equivalent key. Complexity is
   * logarithmic in the size of the container.
   * @param key The key of the element to insert
   * @param value The value of the element to insert
   * @return Returns a pair consisting of a reference to the to the mapped value
   * of the inserted element (or to the element that prevented the insertion)
   * and a bool denoting whether the insertion took place
   */
  std::pair<V &, bool> insert(K &&key, const V &value) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(std::move(key), value);
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return {newNode->value(), true};
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(std::move(key), value);
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return {newNode->value(), true};
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(std::move(key), value));

      fixInsert(root());
      _size += 1;

      return {root()->value(), true};
    } else {
      return {node->value(), false};
    }
  }

  /**
   * Inserts element into the container, if the container doesn't already
   * contains an element with an equivalent key. Complexity is
   * logarithmic in the size of the container.
   * @param key The key of the element to insert
   * @param value The value of the element to insert
   * @return Returns a pair consisting of a reference to the to the mapped value
   * of the inserted element (or to the element that prevented the insertion)
   * and a bool denoting whether the insertion took place
   */
  std::pair<V &, bool> insert(const K &key, V &&value) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(key, std::move(value));
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return {newNode->value(), true};
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(key, std::move(value));
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return {newNode->value(), true};
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(key, std::move(value)));

      fixInsert(root());
      _size += 1;

      return {root()->value(), true};
    } else {
      return {node->value(), false};
    }
  }

  /**
   * Inserts element into the container, if the container doesn't already
   * contains an element with an equivalent key. Complexity is
   * logarithmic in the size of the container.
   * @param key The key of the element to insert
   * @param value The value of the element to insert
   * @return Returns a pair consisting of a reference to the to the mapped value
   * of the inserted element (or to the element that prevented the insertion)
   * and a bool denoting whether the insertion took place
   */
  std::pair<V &, bool> insert(K &&key, V &&value) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(std::move(key), std::move(value));
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return {newNode->value(), true};
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(std::move(key), std::move(value));
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return {newNode->value(), true};
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(std::move(key), std::move(value)));

      fixInsert(root());
      _size += 1;

      return {root()->value(), true};
    } else {
      return {node->value(), false};
    }
  }

  /**
   * Inserts element into the container, or assigns contained element a new
   * value if the container already contains an element with an equivalent key.
   * Complexity is logarithmic in the size of the container.
   * @param key The key of the element to insert
   * @param value The value of the element to insert
   * @return Returns a reference to the to the mapped value
   * of the inserted element.
   */
  V &push(const K &key, const V &value) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(key, value);
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(key, value);
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(key, value));

      fixInsert(root());
      _size += 1;

      return root()->value();
    } else {
      node->assign(value);

      return node->value();
    }
  }

  /**
   * Inserts element into the container, or assigns contained element a new
   * value if the container already contains an element with an equivalent key.
   * Complexity is logarithmic in the size of the container.
   * @param key The key of the element to insert
   * @param value The value of the element to insert
   * @return Returns a reference to the to the mapped value
   * of the inserted element.
   */
  V &push(K &&key, const V &value) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(std::move(key), value);
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(std::move(key), value);
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(std::move(key), value));

      fixInsert(root());
      _size += 1;

      return root()->value();
    } else {
      node->assign(value);

      return node->value();
    }
  }

  /**
   * Inserts element into the container, or assigns contained element a new
   * value if the container already contains an element with an equivalent key.
   * Complexity is logarithmic in the size of the container.
   * @param key The key of the element to insert
   * @param value The value of the element to insert
   * @return Returns a reference to the to the mapped value
   * of the inserted element.
   */
  V &push(const K &key, V &&value) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(key, std::move(value));
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(key, std::move(value));
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(key, std::move(value)));

      fixInsert(root());
      _size += 1;

      return root()->value();
    } else {
      node->assign(std::move(value));

      return node->value();
    }
  }

  /**
   * Inserts element into the container, or assigns contained element a new
   * value if the container already contains an element with an equivalent key.
   * Complexity is logarithmic in the size of the container.
   * @param key The key of the element to insert
   * @param value The value of the element to insert
   * @return Returns a reference to the to the mapped value
   * of the inserted element.
   */
  V &push(K &&key, V &&value) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(std::move(key), std::move(value));
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(std::move(key), std::move(value));
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(std::move(key), std::move(value)));

      fixInsert(root());
      _size += 1;

      return root()->value();
    } else {
      node->assign(std::move(value));

      return node->value();
    }
  }

  /**
   * Removes the element (if one exists) with the key equivalent to the
   * specified key. Complexity is logarithmic in the size of the container.
   * @param key Key value of the element to remove
   * @return true if the removal took place, otherwise false
   */
  bool erase(const K &key) {
    auto [position, node] = findNode(key);

    if (position != 0 or node == nullptr) {
      return false;
    }

    while (node->hasLeft()) {
      auto [newPosition, newNode] = findNode(key, node->left());
      swapNodes(node, newNode);
    }

    fixErase(node);
    _size -= 1;

    delete node;
    return true;
  }

  /**
   * Returns a reference to the value that is mapped to a key equivalent to the
   * specified key, performing an insertion if such key does not already exist.
   * Complexity is logarithmic in the size of the container.
   * @param key The key of the element to find
   * @return A reference to the mapped value of the existing element (or the new
   * element) whose key is equivalent to the specified key
   */
  V &operator[](const K &key) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(key);
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(key);
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(key));

      fixInsert(root());
      _size += 1;

      return root()->value();
    } else {
      return node->value();
    }
  }

  /**
   * Returns a reference to the value that is mapped to a key equivalent to the
   * specified key, performing an insertion if such key does not already exist.
   * Complexity is logarithmic in the size of the container.
   * @param key The key of the element to find
   * @return A reference to the mapped value of the existing element (or the new
   * element) whose key is equivalent to the specified key
   */
  V &operator[](K &&key) {
    auto [position, node] = findNode(key);

    if (position < 0) {
      auto *newNode = new Node<K, V>(std::move(key));
      node->setLeft(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (position > 0) {
      auto *newNode = new Node<K, V>(std::move(key));
      node->setRight(newNode);

      fixInsert(newNode);
      _size += 1;

      return newNode->value();
    } else if (node == nullptr) {
      setRoot(new Node<K, V>(std::move(key)));

      fixInsert(root());
      _size += 1;

      return root()->value();
    } else {
      return node->value();
    }
  }

protected:
  /**
   * Compares two given keys using the comparator function object.
   * @param first The first key
   * @param second The second key
   * @return true if the first key is less than the second key, otherwise false
   */
  bool isLessThan(const K &first, const K &second) { return _less(first, second); }

  /**
   * Compares the keys of two given nodes using the comparator function object.
   * @param first The first node
   * @param second The second node
   * @return true if the key of the first node is less than the key of the
   * second node, otherwise false
   */
  bool isLessThan(const Node<K, V> *first, const Node<K, V> *second) {
    return _less(first->key(), second->key());
  }

  /**
   * Finds a node with a key equivalent to the specified key. If such node is
   * not present in the subtree rooted at the given node, find a possible parent
   * node for a node with specified key. Complexity is logarithmic in the size
   * of the subtree.
   * @param key The key of the node to find
   * @param node The node the search starts from
   * @return A pair consisting of the direction of a node (negative if the node
   * is to the left of the returned node, positive if the node is to the right
   * of the returned node, 0 if the key of the returned node is equivalent to
   * the specified key or if the tree is empty) and the node
   */
  std::pair<int, Node<K, V> *> findNode(const K &key) { return findNode(key, root()); }

  /**
   * Finds a node with a key equivalent to the specified key. If such node is
   * not present in the subtree rooted at the given node, find a possible parent
   * node for a node with specified key. Complexity is logarithmic in the size
   * of the subtree.
   * @param key The key of the node to find
   * @param node The node the search starts from
   * @return A pair consisting of the direction of a node (negative if the node
   * is to the left of the returned node, positive if the node is to the right
   * of the returned node, 0 if the key of the returned node is equivalent to
   * the specified key or if the tree is empty) and the node
   */
  std::pair<int, const Node<K, V> *> findNode(const K &key) const { return findNode(key, root()); }

  /**
   * Finds a node with a key equivalent to the specified key. If such node is
   * not present in the subtree rooted at the given node, find a possible parent
   * node for a node with specified key. Complexity is logarithmic in the size
   * of the subtree.
   * @param key The key of the node to find
   * @param node The node the search starts from
   * @return A pair consisting of the direction of a node (negative if the node
   * is to the left of the returned node, positive if the node is to the right
   * of the returned node, 0 if the key of the returned node is equivalent to
   * the specified key or if the tree is empty) and the node
   */
  std::pair<int, const Node<K, V> *> findNode(const K &key, const Node<K, V> *node) const {
    if (node == nullptr) {
      return {0, nullptr};
    }

    while (true) {
      if (isLessThan(key, node->key())) {
        if (node->hasLeft()) {
          node = node->left();
        } else {
          return {-1, node};
        }
      } else if (isLessThan(node->key(), key)) {
        if (node->hasRight()) {
          node = node->right();
        } else {
          return {1, node};
        }
      } else {
        return {0, node};
      }
    }
  }

  /**
   * Finds a node with a key equivalent to the specified key. If such node is
   * not present in the subtree rooted at the given node, find a possible parent
   * node for a node with specified key. Complexity is logarithmic in the size
   * of the subtree.
   * @param key The key of the node to find
   * @param node The node the search starts from
   * @return A pair consisting of the direction of a node (negative if the node
   * is to the left of the returned node, positive if the node is to the right
   * of the returned node, 0 if the key of the returned node is equivalent to
   * the specified key or if the tree is empty) and the node
   */
  std::pair<int, Node<K, V> *> findNode(const K &key, Node<K, V> *node) {
    if (node == nullptr) {
      return {0, nullptr};
    }

    while (true) {
      if (isLessThan(key, node->key())) {
        if (node->hasLeft()) {
          node = node->left();
        } else {
          return {-1, node};
        }
      } else if (isLessThan(node->key(), key)) {
        if (node->hasRight()) {
          node = node->right();
        } else {
          return {1, node};
        }
      } else {
        return {0, node};
      }
    }
  }

  /**
   * Returns the current root node of the tree.
   * @return The current root node of the tree
   */
  [[nodiscard]] const Node<K, V> *root() const { return _root; }

  /**
   * Returns the current root node of the tree.
   * @return The current root node of the tree
   */
  [[nodiscard]] Node<K, V> *root() { return _root; }

  /**
   * Detaches the root node from the tree. Complexity is constant.
   * @return The former root node
   */
  Node<K, V> *popRoot() {
    Node<K, V> *ptr = nullptr;
    std::swap(ptr, _root);
    return ptr;
  }

  /**
   * Sets given node as the root of the tree. Erases all the nodes that are
   * already present in the tree. Complexity is constant (ignoring erasure).
   * @param node New root of the tree.
   */
  void setRoot(Node<K, V> *node) {
    delete _root;
    _root = node;
  }

  /**
   * Preforms right tree rotation (in counterclockwise direction) with the given
   * node as root. Complexity is constant.
   * @param node The node the rotation is rooted at
   */
  void rotateLeft(Node<K, V> *node) {
    auto *parent = node->parent();

    bool hasParent = node->hasParent();
    bool isLeftChild;

    if (hasParent) {
      isLeftChild = node == parent->left();

      if (isLeftChild) {
        parent->popLeft();
      } else {
        parent->popRight();
      }
    } else {
      popRoot();
    }

    auto *temp = node->popRight();
    node->setRight(temp->popLeft());
    temp->setLeft(node);

    if (hasParent) {
      if (isLeftChild) {
        parent->setLeft(temp);
      } else {
        parent->setRight(temp);
      }
    } else {
      setRoot(temp);
    }
  }

  /**
   * Preforms right tree rotation (in clockwise direction) with the given node
   * as root. Complexity is constant.
   * @param node The node the rotation is rooted at
   */
  void rotateRight(Node<K, V> *node) {
    auto *parent = node->parent();

    bool hasParent = node->hasParent();
    bool isLeftChild;

    if (hasParent) {
      isLeftChild = node == parent->left();

      if (isLeftChild) {
        parent->popLeft();
      } else {
        parent->popRight();
      }
    } else {
      popRoot();
    }

    auto *temp = node->popLeft();
    node->setLeft(temp->popRight());
    temp->setRight(node);

    if (hasParent) {
      if (isLeftChild) {
        parent->setLeft(temp);
      } else {
        parent->setRight(temp);
      }
    } else {
      setRoot(temp);
    }
  }

  /**
   * Swaps two nodes in the tree preserving their colors. Complexity is
   * constant.
   * @param first First node
   * @param second Second node
   */
  void swapNodes(Node<K, V> *first, Node<K, V> *second) {
    auto *firstParent = first->parent();
    bool isFirstLeft = first->isLeftChild();

    if (firstParent != nullptr) {
      if (isFirstLeft) {
        firstParent->popLeft();
      } else {
        firstParent->popRight();
      }
    } else {
      popRoot();
    }

    auto *secondParent = second->parent();
    bool isSecondLeft = second->isLeftChild();

    if (secondParent != nullptr) {
      if (isSecondLeft) {
        secondParent->popLeft();
      } else {
        secondParent->popRight();
      }
    } else {
      popRoot();
    }

    auto [firstLeft, firstRight] = first->popChildren();
    auto [secondLeft, secondRight] = second->popChildren();

    first->setChildren(secondLeft, secondRight);
    second->setChildren(firstLeft, firstRight);

    first->swapColors(second);

    if (firstParent == second) {
      firstParent = first;
    }

    if (secondParent == first) {
      secondParent = second;
    }

    if (firstParent != nullptr) {
      if (isFirstLeft) {
        firstParent->setLeft(second);
      } else {
        firstParent->setRight(second);
      }
    } else {
      setRoot(second);
    }

    if (secondParent != nullptr) {
      if (isSecondLeft) {
        secondParent->setLeft(first);
      } else {
        secondParent->setRight(first);
      }
    } else {
      setRoot(first);
    }
  }

  /**
   * Fixes the balance of the tree assuming that specified node has been
   * inserted by another method. Complexity is logarithmic in the size of the
   * container.
   * @param node The node that has been inserted
   */
  void fixInsert(Node<K, V> *node) {
    auto *parent = node->parent();

    while (node->isRed() && parent != nullptr && parent->isRed()) {
      auto *grand = parent->parent();
      auto *uncle = parent == grand->left() ? grand->right() : grand->left();

      if (uncle != nullptr && uncle->isRed()) {
        parent->makeBlack();
        uncle->makeBlack();
        grand->makeRed();

        node = grand;
      } else if (parent == grand->left()) {
        if (node == parent->right()) {
          rotateLeft(parent);
          node = parent;
          parent = node->parent();
        }

        rotateRight(grand);
        parent->swapColors(grand);
        node = parent;
      } else {
        if (node == parent->left()) {
          rotateRight(parent);
          node = parent;
          parent = node->parent();
        }

        rotateLeft(grand);
        parent->swapColors(grand);
        node = parent;
      }

      parent = node->parent();
    }

    root()->makeBlack();
  }

  /**
   * Detaches the specified node from the tree and fixes the balance of the tree
   * assuming that specified node has only one child. Complexity is logarithmic
   * in the size of the container.
   * @param node The node to be detached
   */
  void fixErase(Node<K, V> *node) {
    auto *parent = node->parent();
    auto *child = node->hasLeft() ? node->popLeft() : node->popRight();

    if (parent == nullptr) {
      popRoot();
      setRoot(child);

      if (child != nullptr) {
        child->makeBlack();
      }

      return;
    }

    Node<K, V> *sibling;

    if (node->isLeftChild()) {
      sibling = parent->right();

      parent->popLeft();
      parent->setLeft(child);
    } else {
      sibling = parent->left();

      parent->popRight();
      parent->setRight(child);
    }

    if (node->isBlack()) {
      node = child;

      if (node != nullptr and node->isRed()) {
        node->makeBlack();
      } else {
        if (node != nullptr) {
          node->makeBlack();
        }

        while (parent != nullptr and (node == nullptr or node->isBlack())) {
          if (node != nullptr) {
            sibling = node->isLeftChild() ? parent->right() : parent->left();
          }

          if (sibling->isRed()) {
            sibling->makeBlack();
            parent->makeRed();

            if (sibling->isLeftChild()) {
              rotateRight(parent);
              sibling = parent->left();
            } else {
              rotateLeft(parent);
              sibling = parent->right();
            }
          }

          if ((sibling->left() == nullptr or sibling->left()->isBlack()) and
              (sibling->right() == nullptr or sibling->right()->isBlack())) {
            sibling->makeRed();

            if (parent->isRed()) {
              parent->makeBlack();
              break;
            } else {
              parent->makeBlack();
              node = parent;
              parent = parent->parent();
            }
          } else {
            if (sibling->isLeftChild()) {
              if (sibling->left() == nullptr or sibling->left()->isBlack()) {
                sibling->right()->makeBlack();
                sibling->makeRed();
                rotateLeft(sibling);
                sibling = parent->left();
              }

              if (node != nullptr) {
                node->makeBlack();
              }

              sibling->copyColor(parent);
              parent->makeBlack();
              sibling->left()->makeBlack();
              rotateRight(parent);
              break;
            } else {
              if (sibling->right() == nullptr or sibling->right()->isBlack()) {
                sibling->left()->makeBlack();
                sibling->makeRed();
                rotateRight(sibling);
                sibling = parent->right();
              }

              if (node != nullptr) {
                node->makeBlack();
              }

              sibling->copyColor(parent);
              parent->makeBlack();
              sibling->right()->makeBlack();
              rotateLeft(parent);
              break;
            }
          }
        }
      }
    }
  }

private:
  /**
   * The root node ot the tree, or nullptr if tree is empty.
   */
  Node<K, V> *_root;

  /**
   * The number of nodes in the tree.
   */
  std::size_t _size;

  /**
   * The function object that compares the keys.
   */
  Compare _less{};
};

#endif // RABBIT_TREE_HPP
