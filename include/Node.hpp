#ifndef RABBIT_NODE_HPP
#define RABBIT_NODE_HPP

#include <cstdint>
#include <utility>

template <class K, class V> class Node {
public:
  explicit Node(const K &key) : Node(key, V{}) {}

  explicit Node(K &&key) : Node(std::move(key), V{}) {}

  explicit Node(const K &key, const V &value)
      : _key(key), _value(value), _left(nullptr), _right(nullptr), _parent(nullptr),
        _isBlack(false) {}

  explicit Node(K &&key, const V &value)
      : _key(std::move(key)), _value(value), _left(nullptr), _right(nullptr), _parent(nullptr),
        _isBlack(false) {}

  explicit Node(const K &key, V &&value)
      : _key(key), _value(std::move(value)), _left(nullptr), _right(nullptr), _parent(nullptr),
        _isBlack(false) {}

  explicit Node(K &&key, V &&value)
      : _key(std::move(key)), _value(std::move(value)), _left(nullptr), _right(nullptr),
        _parent(nullptr), _isBlack(false) {}

  ~Node() {
    delete _left;
    delete _right;
  }

  void assign(const V &value) { _value = value; }

  void assign(V &&value) { _value = std::move(value); }

  [[nodiscard]] const V &value() const { return _value; }

  [[nodiscard]] V &value() { return _value; }

  [[nodiscard]] const K &key() const { return _key; }

  [[nodiscard]] bool isRed() const { return not _isBlack; }

  [[nodiscard]] bool isBlack() const { return _isBlack; }

  void makeRed() { _isBlack = false; }

  void makeBlack() { _isBlack = true; }

  void flipColor() { _isBlack = not _isBlack; }

  void copyColor(const Node<K, V> *node) { _isBlack = node->_isBlack; }

  void swapColors(Node<K, V> *node) { std::swap(_isBlack, node->_isBlack); }

  [[nodiscard]] bool hasChildren() const { return hasLeft() or hasRight(); }

  [[nodiscard]] bool hasParent() const { return _parent != nullptr; }

  [[nodiscard]] bool hasLeft() const { return _left != nullptr; }

  [[nodiscard]] bool hasRight() const { return _right != nullptr; }

  [[nodiscard]] bool isLeftChild() const { return _parent != nullptr and this == _parent->_left; }

  [[nodiscard]] bool isRightChild() const { return _parent != nullptr and this == _parent->_right; }

  [[nodiscard]] Node<K, V> *parent() const { return _parent; }

  [[nodiscard]] Node<K, V> *left() const { return _left; }

  [[nodiscard]] Node<K, V> *right() const { return _right; }

  [[nodiscard]] std::pair<Node<K, V> *, Node<K, V> *> children() const { return {left(), right()}; }

  Node<K, V> *popLeft() {
    Node<K, V> *ptr = nullptr;
    std::swap(ptr, _left);

    if (ptr != nullptr) {
      ptr->_parent = nullptr;
    }

    return ptr;
  }

  Node<K, V> *popRight() {
    Node<K, V> *ptr = nullptr;
    std::swap(ptr, _right);

    if (ptr != nullptr) {
      ptr->_parent = nullptr;
    }

    return ptr;
  }

  [[nodiscard]] std::pair<Node<K, V> *, Node<K, V> *> popChildren() {
    return {popLeft(), popRight()};
  }

  void setLeft(Node<K, V> *node) {
    delete _left;
    _left = node;

    if (node != nullptr) {
      node->_parent = this;
    }
  }

  void setRight(Node<K, V> *node) {
    delete _right;
    _right = node;

    if (node != nullptr) {
      node->_parent = this;
    }
  }

  void setChildren(Node<K, V> *left, Node<K, V> *right) {
    setLeft(left);
    setRight(right);
  }

  [[nodiscard]] Node<K, V> *copy() const {
    auto *node = new Node<K, V>(key(), value());
    node->copyColor(this);

    if (hasLeft()) {
      node->setLeft(left()->copy());
    }

    if (hasRight()) {
      node->setRight(right()->copy());
    }

    return node;
  }

private:
  const K _key;
  V _value;

  Node<K, V> *_left;
  Node<K, V> *_right;
  Node<K, V> *_parent;

  bool _isBlack;
};

#endif // RABBIT_NODE_HPP
